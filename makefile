

all: main

main: main.c
	gcc -g -Wall -o main main.c -lncurses
	
.PHONY: clean

clean:
	rm -f *.o main
