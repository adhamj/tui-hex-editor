#include <ncurses.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#define MAX_WIDTH 61
#define MIN_HEIGHT 2
#define MIN_WIDTH 12
#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1
#define ROW_SIZE 67
#define RUNNING 1
/*--------------------------------(Structs & Enums)------------------------------------------*/
typedef struct Cursor{
    int x_pos;
    int y_pos;
}cursor;
enum Mode{
    Read,
    Write
};
struct Cursor my_cursor = {0,0};
struct stat my_stat;
enum Mode cur_mode;
/*--------------------------------(Variable Declaration)--------------------------------------*/
int steps[] = { 12,13, 15,16, 18,19, 21,22, 
                25,26, 28,29, 31,32, 34,35,
                38,39, 41,42, 44,45, 47,48,
                51,52, 54,55, 57,58, 60,61 };   /* legal x positions for cursor */ 
int g_width;                                    /* global width variable */
int g_height;                                   /* global height variable */
int g_file;                                     /* global file descriptor */
int mod = 0;                                    /* step count keeper */
int y_ref = 0;                                  /* where to start rendering*/
int line_size = 16;                             /* amount of bytes to display*/
char* map;                                      /* a pointer to the mapped data*/
off_t size;                       
/*--------------------------------(Function Declaration)--------------------------------------*/
int  get_hex(int input);
void mv_up(int count, bool key);
void mv_down(int count, bool key);
void mv_left(int count);
void mv_right(int count);
void render_window();
void init_editor(int argc, char *argv[]);
void manage_cursor();
void insert(int input);
void go_to_line();
void check_window_resize();
void _exit(int exit_code);
/*----------------------------------(Main Loop)---------------------------------------*/
int main(int argc, char *argv[]){
    init_editor(argc , argv);
    int input;
    do{
        input = getch();
        switch(input){
            case KEY_UP:
                mv_up(1,FALSE);
                break;
            case KEY_DOWN:
                mv_down(1,FALSE);
                break;
            case KEY_RIGHT:
                mv_right(1);
                break;
            case KEY_LEFT:
                mv_left(1);
                break;
            case KEY_PPAGE:
                mv_up(g_height-2*MIN_HEIGHT,TRUE);
                break;
            case KEY_NPAGE:
                mv_down(g_height-2*MIN_HEIGHT,TRUE);
                break;
            case KEY_F(1):
                _exit(EXIT_SUCCESS);
                break;
            case KEY_F(2):
                if(cur_mode == Write){
                    cur_mode = Read;
                }
                else{
                    cur_mode = Write;
                }
                break;
            case KEY_F(3):
                go_to_line();
                break;
            default:
                if(cur_mode == Write){
                    insert(input);
                }
                break;
        }
        manage_cursor();
        check_window_resize();
        render_window();
    } while(RUNNING);
}
/*-----------------------------(Function Implementation)------------------------------*/
void mv_up(int count,bool key){
    if( key && (y_ref-= count) < 0){
        y_ref = 0;
    }
    else{
        my_cursor.y_pos-= count;
    }
}
void mv_down(int count,bool key){
    if( key && (y_ref+= count) > size){
        y_ref+= size - count;
    }
    else{
        my_cursor.y_pos+= count;
    }
}
void mv_left(int count){
    mod -= count;
    if(mod < 0 ){
        mod = 0;
    }
    my_cursor.x_pos = steps[mod];
}
void mv_right(int count){
    mod += count;
    if(mod > (2*line_size - 1)){
        mod = 2*line_size - 1;
    }
    my_cursor.x_pos = steps[mod];
}
void render_window(){
    int curr_y;
    int curr_x;
    int temp;
    move(0, 0);
    printw(" offset");
    for(int k=0 ; k < line_size ; k++){
        mvprintw(0,steps[2*k],"%02X",k);
    }
    printw("    ASCII");
    move(MIN_HEIGHT,0);
    for( int j = y_ref; j < g_height + y_ref -3; j++){
        printw("%08X   ", j*line_size);
        for(int i=0 ; (i < line_size) && ((i+ j*line_size ) <= size); i++){
            temp = (0x000000FF & (int)map[i + j*line_size] );/* isolating byte, had problems with printing*/
            getyx(stdscr,curr_y,curr_x);
            if( i%4 == 0 ){
                if( (steps[mod] == steps[i*2]
                    || steps[mod] == steps[i*2 + 1])
                    && (j + MIN_HEIGHT == my_cursor.y_pos + y_ref)){
                    
                    attron(COLOR_PAIR(1));
                }
                move(curr_y,steps[line_size*2 - 1] + i + 3);
                ( temp > 32 ) ? printw("%c",temp) : printw(".");
                attroff(COLOR_PAIR(1));
                move(curr_y, curr_x);
                printw(" %02X ",temp);
            }
            else{
                if( (steps[mod] == steps[i*2]
                    || steps[mod] == steps[i*2 + 1])
                    && (j + MIN_HEIGHT == my_cursor.y_pos + y_ref)){
                    
                    attron(COLOR_PAIR(1));
                }
                move(curr_y,steps[line_size*2 - 1] + i + 3);
                ( temp > 32 ) ? printw("%c",temp) : printw(".");
                attroff(COLOR_PAIR(1));
                move(curr_y,curr_x);
                printw("%02X ",temp);
            }
        }
        move(curr_y+1,0);
    }
    getyx(stdscr,curr_y,curr_x);
    attron(COLOR_PAIR(1));
    mvprintw(curr_y,6,"QUIT <F1> | ");
    printw("READ_WRITE_TOGGLE <F2> (MODE:%s) | ", (cur_mode == Read) ? "  READ" : " WRITE");
    printw("GO_TO_LINE( DEC ) <F3> ");
    attroff(COLOR_PAIR(1));
    move(my_cursor.y_pos, my_cursor.x_pos);
    refresh();
}
void init_editor(int argc, char *argv[]){
    if(argc != 2){
        printf("Usage: %s <a c file name>\n", argv[0]);
        _exit(EXIT_FAILURE);
    }
    g_file = open(argv[1], O_RDWR);
    if(g_file < 0){
        perror("Cannot open input file");
        _exit(EXIT_FAILURE);
    }
    if( fstat(g_file, &my_stat) ){
        mvprintw(g_height/2,(g_width-5)/2,"fstat Error!!!");
        _exit(EXIT_FAILURE);
    }
    size = my_stat.st_size;
    initscr();                                  
    noecho();
    raw();
    start_color();
    keypad(stdscr, TRUE);
    getmaxyx(stdscr, g_height, g_width);     
    if(g_width >= 80){
            line_size = 16;
    } else if(g_width >= 66 && g_width < 80){
        line_size = 12;
    } else if(g_width >= 48 && g_width < 66){
        line_size = 8;
    } else{
        line_size = 4;
    }
    init_pair(1, COLOR_BLACK, COLOR_WHITE);
    map = mmap( NULL , size , PROT_READ | PROT_WRITE , MAP_SHARED , g_file , 0);
    render_window();
    move(2,MIN_WIDTH);
    cur_mode = Read;
}
void manage_cursor(){
    if( my_cursor.x_pos < MIN_WIDTH){
        my_cursor.x_pos = MIN_WIDTH;
    } else if( my_cursor.x_pos > steps[line_size*2 - 1]){
        my_cursor.x_pos = steps[line_size*2 - 1];
    }
    if( my_cursor.y_pos < MIN_HEIGHT){
        my_cursor.y_pos = MIN_HEIGHT;
        if( y_ref > 0 ){
            y_ref--;
        }
    } else if( my_cursor.y_pos > g_height-2){
        my_cursor.y_pos = g_height-2;
        y_ref++;
    }
    if( y_ref + g_height - 3 > size/line_size ) {
        y_ref = size/line_size + 3 - g_height;
    }
}

void insert(int input){
    int new_byte = get_hex(input);
    int old_byte_pos = (mod/2 + (y_ref + my_cursor.y_pos - MIN_HEIGHT)*line_size);
    if(new_byte == -1) return;
    if((mod%2) == 1) new_byte = (map[old_byte_pos] & 0x000000F0 ) | new_byte;
    else new_byte = (map[old_byte_pos] & 0x0000000F ) | (new_byte << 4);
    map[old_byte_pos] = new_byte;
}
int get_hex(int input){
    switch(input){
        case '0': return 0x0;break;
        case '1': return 0x1;break;
        case '2': return 0x2;break;
        case '3': return 0x3;break;
        case '4': return 0x4;break;
        case '5': return 0x5;break;
        case '6': return 0x6;break;
        case '7': return 0x7;break;
        case '8': return 0x8;break;
        case '9': return 0x9;break;
        case 'a':case 'A': return 0xA;break;
        case 'b':case 'B': return 0xB;break;
        case 'c':case 'C': return 0xC;break;
        case 'd':case 'D': return 0xD;break;
        case 'e':case 'E': return 0xE;break;
        case 'f':case 'F': return 0xF;break;
        default: return -1;
    }
}
void go_to_line(){
    int addr;
    int height = 3;
    int width = 40;
    WINDOW* win = newwin(height, width, g_height/2-2, MIN_WIDTH+5);
    box(win, 0, 0);
    mvwprintw(win, height/2, 5, "Insert line in dec: ");
    wrefresh(win);
    move(g_height/2 - 1, MIN_WIDTH+30);
    echo();                            /*print input on screen, strictly for looks*/
    wscanw(win,"%d",&addr);
    noecho();
    wrefresh(win);
    y_ref = addr/line_size;
    delwin(win);
    clear();
}
void check_window_resize(){
    int new_height;
    int new_width;
    getmaxyx(stdscr,new_height,new_width);
    if(g_height != new_height){
        g_height = new_height;
        clear();
    }
    if(g_width != new_width){
        g_width = new_width;
        if(new_width >= 80){
            line_size = 16;
        } else if(new_width >= 66 && new_width < 80){
            line_size = 12;
        } else if(new_width >= 48 && new_width < 66){
            line_size = 8;
        } else{
            line_size = 4;
        }
        clear();
    }
}
void _exit(int exit_code){
    endwin();                                   /* End curses mode */
    munmap(map,size);                            
    close(g_file);
    exit(exit_code);
}
